def bubble_sort(arr):
    """
    Sort the given array in ascending order
    """

    for i in range(len(arr)-1):
        swap = False
        for j in range(len(arr)-1):
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]
                swap = True
        else:
            if swap == False:
                break
        print(arr)
                


if __name__ == '__main__':
    bubble_sort([10,44,45,4,56,5,45])