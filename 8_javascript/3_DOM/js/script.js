var id = 0;

$("#add_task").on('click', () => {
    addTxt();
});

$("#txt").keypress(function(e) {
    if(e.which == 13) {
        addTxt();
    }
});

function addTxt(){
    var txt = $("#txt").val();
    if(txt !== ""){
        var ele = `<li class="collection-item" id="li-${id}"><div><strong>${txt}</strong><a href="#" class="secondary-content" style="margin-right: 10px;" onclick="del(${id})"><i class="material-icons">delete_outline</i></a><a href="#" class="secondary-content" onclick="changeClr(${id})"><i class="material-icons">autorenew</i></a></div></li>`;

        $(ele).insertBefore("#form");
        $("#txt").val("");
        id++;
    }else{
        alert("Enter text to add");
    }
}

function del(id) {
    $(`#li-${id}`).remove();
}

function changeClr(id) {
    $(`#li-${id}`).css('color', `rgb(${getRandom()},${getRandom()},${getRandom()})`);
}

function getRandom() {
    return Math.random() * (225 - 0) + 0;
}