function* fibonacci() {
    let first = 0;
    let second = 1;
    yield first;
    while (true) {
        yield first + second;
        [second, first] = [first, first + second];
    }
}

var it = fibonacci();

var n = prompt("Enter value of n: ")

for(let i=0;i<=n;i++){
    document.write(it.next().value+"<br>");
}