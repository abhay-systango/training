"use strict";

const products = [
    {
        name: 'Bottel',
        price: 250
    },{
        name: 'Bag',
        price: 1050
    },{
        name: 'Laptop',
        price: 90000
    },{
        name: 'Pen Drive',
        price: 550
    },{
        name: 'Cup',
        price: 150
    }
];

function createBucket(products) {

    let bucket = [];

    products.forEach(function (product, index) {
        var name = product.name;
        var price = product.price;
        var quantity = prompt(`Product Name: ${product.name}\nPrice: ${product.price}RS\n\nEnter quantity [0 if don't want to buy]?`);
    
        if(quantity>=1) {
            bucket.push({
                name, price, quantity
            })
        }
    });

    // Only for diplay purpose
    console.log("Initial Bucket");
    bucket.forEach(function(item, index){
        console.log(`${item.name}: ${item.quantity}`)
    });


    return bucket;
}

function updateBucket(bucket) {

    let updatedBucket = []
    bucket.forEach(function (item, index) {
        var quantity = prompt(`Enter the new quantity for ${item.name}\n\nCurrent quantity: ${item.quantity}\nPrice: ${item.price}\nTotal cost: ${item.price*item.quantity}`);
    
        if(quantity=="") {
            updatedBucket.push(item);
        }else if(quantity != 0){
            item.quantity = quantity;
            updatedBucket.push(item);
        }
    });

    // Only for diplay purpose
    console.log("Updated Bucket");
    bucket.forEach(function(item, index){
        console.log(`${item.name}: ${item.quantity}`)
    });

    return updatedBucket;
}

function totalCost(bucket) {

    let total = 0;

    bucket.forEach(function (item, index){
        total += item.price;
    });

    console.log(`Total Cost: ${total}`);
}

totalCost(updateBucket(createBucket(products)));