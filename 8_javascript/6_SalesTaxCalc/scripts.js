let country = [
    {name: 'India', taxPercent: 10},
    {name: 'USA', taxPercent: 15},
    {name: 'Canada', taxPercent: 20},
    {name: 'UK', taxPercent: 5},
];

let products = [
    {product_name: 'Mobile', price: 1000},
    {product_name: 'Switch', price: 1500},
    {product_name: 'Bag', price: 2000},
    {product_name: 'Bottel', price: 500},
];

function taxCalc(price, taxPercent) {
    return ((price * taxPercent) / 100);
}


let result_list = []

for(let i=0; i<=products.length-1;i++) {
    for(let j=0; j<=country.length-1;j++) {
        result_list.push({
            product_name: products[i].product_name,
            product_price: products[i].price,
            product_country: country[j].name,
            product_taxPercent: country[j].taxPercent,
            product_final_price: products[i].price + taxCalc(products[i].price, country[j].taxPercent),
        });
    }
}

result_list.forEach(product => {
    var name = product.product_name;
    var price = product.product_price;
    var country = product.product_country;
    var taxPercent = product.product_taxPercent;
    var finalPrice = product.product_final_price;

    var ele = `<tr><td>${name}</td><td>${price}</td><td>${country}</td><td>${taxPercent}</td><td>${finalPrice}</td></tr>`;

    $("#body").append(ele);
});