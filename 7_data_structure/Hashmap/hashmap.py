names = input("Enter names with spaces: ").split(" ") # ['abhay', 'aman', 'gopal', 'abhay', 'gopal', 'aashish']
names_hash = { key: value for key, value in enumerate(names)}


for name in set(names_hash.values()):
    name_count = list(names_hash.values()).count(name)
    print(name, " = ", name_count)
