def move(rod_1, rod_2):
    """
    This function will move one disc from one place
    to other.
    """

    if len(rod_1) == 0 and len(rod_2) >= 1:
            rod_1.append(rod_2.pop())
    elif len(rod_2) == 0 and len(rod_1) >= 1:
        rod_2.append(rod_1.pop())
    elif len(rod_2) >= 1 and len(rod_1) >= 1:
        if rod_1[-1] > rod_2[-1]:
            rod_1.append(rod_2.pop())
        else:
            rod_2.append(rod_1.pop())

def tower_of_hanoi(initial_rod, extra_rod, end_rod):
    """
    This function will tell the 2 rods on which shifting going to happen
    """

    for i in range(1, moves+1):
        if i%3 == 1:
            move(initial_rod, end_rod)
                
        elif i%3 == 2:
            move(initial_rod, extra_rod)

        elif i%3 == 0:
            move(end_rod, extra_rod)

        print(initial_rod, extra_rod, end_rod)


if __name__ == '__main__':

    discs = int(input("Enter no. of discs: "))

    initial_rod = [i for i in range(discs, 0, -1)]
    extra_rod = []
    end_rod = []

    moves = (2**len(initial_rod))-1

    if moves%2 == 0:    
        tower_of_hanoi(initial_rod, end_rod, extra_rod)
    else:
        tower_of_hanoi(initial_rod, extra_rod, end_rod)
