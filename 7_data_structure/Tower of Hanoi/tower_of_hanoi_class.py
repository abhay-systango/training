class TowerOfHanoi:

    def __init__(self):
        """
        Initialising const with empty extra and end rod
        """

        self.extra_rod = []
        self.end_rod = []

    def move(self, rod_1, rod_2):
        """
        This function will move one disc from one place
        to other.
        """

        if len(rod_1) == 0 and len(rod_2) >= 1:
                rod_1.append(rod_2.pop())
        elif len(rod_2) == 0 and len(rod_1) >= 1:
            rod_2.append(rod_1.pop())
        elif len(rod_2) >= 1 and len(rod_1) >= 1:
            if rod_1[-1] > rod_2[-1]:
                rod_1.append(rod_2.pop())
            else:
                rod_2.append(rod_1.pop())

    def main(self):
        """
        This function will tell the 2 rods on which shifting going to happen
        """

        for i in range(1, self.moves+1):
            if i%3 == 1:
                self.move(self.initial_rod, self.end_rod)
                    
            elif i%3 == 2:
                self.move(self.initial_rod, self.extra_rod)

            elif i%3 == 0:
                self.move(self.end_rod, self.extra_rod)

            print(self.initial_rod, self.extra_rod, self.end_rod)

    def run(self):
        """
        Executing the whole program
        """

        self.discs = int(input("Enter no. of discs: "))
        self.moves = (2**self.discs)-1
        self.initial_rod = [self.disc for self.disc in range(self.discs, 0, -1)]
        self.main()


if __name__ == '__main__':
    
    TowerOfHanoi().run()
