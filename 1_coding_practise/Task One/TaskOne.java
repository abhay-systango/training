import java.io.IOException;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

class TaskOne extends ReadWriteCsv{

    public static List get_item_list(String file_path) throws Exception {

        String row;
        List item_list = new ArrayList();

        BufferedReader csvReader = read_file(file_path);

        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            item_list.add(Arrays.asList(data));
        }
        csvReader.close();

        return item_list;
    }

    public static double getTax(int price, int taxPercent) {

        return (double) ((price * taxPercent) / 100);
    }

    public static List<String> append_data(String file_path) throws IOException {

        List<String> result_list = new ArrayList<String>();

        result_list.add("Product-Name, Product-CostPrice, Country, Product-SalesTax, Product-FinalPrice");


        Scanner scn = new Scanner(System.in);

        System.out.println("Enter the tax percent: ");
        int taxPercent = scn.nextInt();

        try {
            List item_list = get_item_list(file_path);

            for(int i=0;i<=item_list.size()-1;i++){
                
                List item = (List)item_list.get(i);
                int price = Integer.parseInt(((String)item.get(1)).trim());
                double tax = getTax(price, taxPercent);
                String final_item_data = String.join(",", item)+","+taxPercent+","+Double.toString(price+tax);

                result_list.add(final_item_data);
            }
        } catch (Exception e){
            System.out.println(e);
        }
        return result_list;
    }

    public static void run_task(){

        try {
            write_data(append_data("input.csv"));
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public static void main(String args[]){   
        run_task();
    } 
}