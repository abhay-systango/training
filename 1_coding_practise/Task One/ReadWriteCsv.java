import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.io.BufferedReader;

class ReadWriteCsv {

    public static BufferedReader read_file(String file_path) throws Exception {
        FileReader file = new FileReader(file_path);

        BufferedReader csvReader = new BufferedReader(file);
        csvReader.readLine();
        return csvReader;
    }

    public static void write_data(List<String> result_list) throws IOException {
        
        FileWriter writeCsv = new FileWriter("output.csv");

        for(int i=0;i<=result_list.size()-1;i++) {
            writeCsv.write(result_list.get(i));
            writeCsv.write(System.getProperty("line.separator"));
        }

        writeCsv.close();
    }
}