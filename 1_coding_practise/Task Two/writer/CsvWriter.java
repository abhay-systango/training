package writer;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;


public class CsvWriter
{
    public static void writeData(List<String> resultList) throws IOException {

        Scanner scn = new Scanner(System.in);

        System.out.println("Enter name of output file: ");
        String filePath = scn.nextLine();
        
        FileWriter writeCsv = new FileWriter(filePath);

        for(int i=0;i<=resultList.size()-1;i++) {
            writeCsv.write(resultList.get(i));
            writeCsv.write(System.getProperty("line.separator"));
        }

        writeCsv.close();
    }
}