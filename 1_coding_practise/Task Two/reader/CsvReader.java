package reader;

import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class CsvReader implements ReaderInterface
{
    public static List<List> bufferToList(BufferedReader data) throws IOException{

        String row;

        // Creating a empty list
        List<List> itemList = new ArrayList<List>();

        // Reading from a Buffer and getting them as a list
        while ((row = data.readLine()) != null) {
            String[] itemData = row.split(",");

            // Checking if the no. of columns in every row is same
            if(Arrays.asList(itemData).size()!=4) {
                System.out.println("There should be exactly 4 columns but there are "+Arrays.asList(itemData).size()+" columns.");
                continue;
            }
            itemList.add(Arrays.asList(itemData));
        }
        data.close();

        return itemList;
    }
    
    public List<List> readFile() {

        // Taking input file from user
        Scanner scn = new Scanner(System.in);
        System.out.println("Enter name of input file: ");

        String filePath = scn.nextLine();

        try {
            // Reading file and storing it as stream
            FileReader file = new FileReader(filePath);
            BufferedReader csvReader = new BufferedReader(file);
            
            // trying to read file
            try {
                csvReader.readLine();
            } catch (IOException e) {
                System.out.print("Looks like there is no data in your csv file");
            }
            
            // Converting buffer to list
            try {
                return (bufferToList(csvReader));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }


        // If list is not generated then passing an empty list
        return new ArrayList<List>();
    }
}