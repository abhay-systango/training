package reader;

import java.util.List;

public interface ReaderInterface {
    public List<List> readFile();
}