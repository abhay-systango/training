import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import reader.CsvReader;
import writer.CsvWriter;

class SalesTax {

    public static double getTax(int price, int taxPercent) {

        return (double) ((price * taxPercent) / 100);
    }

    public static List<String> appendData(List<List> itemList){

        List<String> resultList = new ArrayList<String>();

        resultList.add("Product-Name, Product-CostPrice, Product-SalesTax, Country, Product-FinalPrice");

        try {
            for(int i=0;i<=itemList.size()-1;i++){
                
                List item = (List)itemList.get(i);
                int price = Integer.parseInt(((String)item.get(1)).trim());
                int taxPercent = Integer.parseInt(((String)item.get(2)).trim());
                double tax = getTax(price, taxPercent);
                String finalItemData = String.join(",", item)+","+Double.toString(price+tax);

                resultList.add(finalItemData);
            }
        } catch (Exception e){
            System.out.println(e);
        }
        return resultList;
    }

    public static void runTask(){
        try {
            // Reading and getting data in list
            CsvReader obj = new CsvReader();
            List itemList = obj.readFile();

            // Appending data to list
            List<String> updatedList = appendData(itemList);

            // Writing data to csv
            CsvWriter.writeData(updatedList);
        } catch (FileNotFoundException e) {
            System.out.println("Sorry File is not there");
        } catch (IOException e) {
            System.out.println("Some problem occured while reading or closing the file");
        }
    }

    public static void main(String args[]){   
        runTask();
    }
}